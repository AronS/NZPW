import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.text.DecimalFormat;


public class GUI extends Application{

    public static void main (String[] args) throws ClassNotFoundException {
    launch(args);
}

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Całka");
        BorderPane layoutGlowny = new BorderPane();

        layoutGlowny.setPrefSize(400,400);

        Label calka = new Label ("Całka z: ");
        Label granicaGorna = new Label("Górna granica całkowania: ");
        Label granicaDolna = new Label("Dolna granica całkowania: ");
        Label wynik = new Label("");
        Font czcionka = new Font("Arial", 20);
        wynik.setFont(czcionka);
        TextField calkaTekst = new TextField();
        TextField granicaGornaTekst = new TextField();
        granicaGornaTekst.setMinWidth(100);
        TextField granicaDolnaTekst = new TextField();
        Button oblicz = new Button("Oblicz");
        Button wykres = new Button ("Wygeneruj wykres");

        StackPane panelGorny = new StackPane();
        VBox panelLewy = new VBox(10);
        VBox panelSrodkowy = new VBox(10);
        HBox panelDolny = new HBox(10);

        layoutGlowny.setTop(panelGorny);
        layoutGlowny.setLeft(panelLewy);
        layoutGlowny.setCenter(panelSrodkowy);
        layoutGlowny.setBottom(panelDolny);
        layoutGlowny.setMargin(panelGorny,new Insets(40,10,10,10));
        layoutGlowny.setMargin(panelLewy,new Insets(10,10,10,10));
        layoutGlowny.setMargin(panelSrodkowy,new Insets(10,10,10,10));
        layoutGlowny.setMargin(panelDolny,new Insets(10,10,10,10));


        panelGorny.getChildren().addAll(wynik);

        panelLewy.setAlignment(Pos.CENTER_RIGHT);
        panelLewy.getChildren().addAll(calka, granicaGorna, granicaDolna);

        panelSrodkowy.setAlignment(Pos.CENTER);
        panelSrodkowy.getChildren().addAll(calkaTekst, granicaGornaTekst, granicaDolnaTekst);

        panelDolny.setAlignment(Pos.CENTER);
        panelDolny.getChildren().addAll(oblicz,wykres);


        Scene scena = new Scene(layoutGlowny);
        primaryStage.setScene(scena);
        primaryStage.setResizable(false);
        primaryStage.show();

        DecimalFormat df = new DecimalFormat("0.00");
        oblicz.setOnAction(e ->
        wynik.setText("Wynik całkowania to: "+df.format(new Oblicz().oblicz(
                Double.parseDouble(granicaGornaTekst.getText()),
                Double.parseDouble(granicaDolnaTekst.getText()),
                calkaTekst.getText()))));



        wykres.setOnAction(e -> new Oblicz().wykres(calkaTekst));

    }
}
