import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import java.sql.*;


public class Oblicz{


    public double oblicz(double gorna, double dolna, String calka){
        double wynik = 0;
        try {

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/wykres",
                    "root", "");

            double dx = (gorna - dolna)/100;
            for (int i=1; i<=100; i++ ){
                double wynikPrzedzialu = funkcja(dolna+i*dx, calka);
                wynik = wynik+wynikPrzedzialu;
                double osX = dolna+i*dx;
                double osY = wynikPrzedzialu;
                con.createStatement().execute("UPDATE xy SET x="+osX+", y="+osY+" WHERE id="+i+"");
            }

            wynik = wynik*dx;

        } catch (SQLException ex){ System.out.println("Nie udało połączyć się z bazą danych"); }

        return wynik;
    }

    public double funkcja(double x, String calka){
        Expression e = new ExpressionBuilder(calka).variable("x")
                .build().setVariable("x",x);
        double result = e.evaluate();
        return result;
    }

    public void wykres(TextField calkaTytul) {
        Stage secondStage = new Stage();
        secondStage.setTitle("Wykres");

        NumberAxis osX = new NumberAxis();
        NumberAxis osY = new NumberAxis();
        LineChart<Number, Number> wykres = new LineChart<Number, Number>(osX, osY);
        wykres.setTitle("Wykres całki: "+calkaTytul.getText());
        wykres.setCreateSymbols(false);
        XYChart.Series series = new XYChart.Series();

        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/wykres",
                    "root", "");
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery("SELECT x, y FROM xy");
            while (rs.next()) {
                double x = rs.getDouble("x");
                double y = rs.getDouble("y");
                series.getData().add(new XYChart.Data(x, y));
            }

        }catch (SQLException ex){
            System.out.println("Wykres: nie udało się połączyć z bazą");}

        Scene scenaWykres = new Scene(wykres,500,400);
        wykres.getData().add(series);
        secondStage.setScene(scenaWykres);
        secondStage.show();


    }

}
